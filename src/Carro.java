public class Carro {
    //ATRIBUTOS
    private String color;
    private double vel_maxima;
    private String modelo;
    private String placa;

    //CONSTRUCTOR
    public Carro(String color, double vel_maxima, String modelo, String placa){
        this.color = color;
        this.vel_maxima = vel_maxima;
        this.modelo = modelo;
        this.placa = placa;
    }

    /****************
     * CONSULTORES
     * GETTERS
     ***************/

    public String getColor() {
        return color;
    }

    public double getVel_maxima() {
        return vel_maxima;
    }

    public String getModelo() {
        return modelo;
    }

    public String getPlaca() {
        return placa;
    }

    /******************
     * MODIFICADORES
     * SETTERS
     *****************/

    public void setColor(String color) {
        this.color = color;
    }

    public void setVel_maxima(double vel_maxima) {
        this.vel_maxima = vel_maxima;
    }

    //ACCIONES
    public void acelerar(){
        System.out.println("Acelerando...🚘");
    }
    public void frenar(){
        System.out.println("Frenando...⏹");
    }
    public void girar_dere(){
        System.out.println("-->");
    }
    public void girar_izq(){
        System.out.println("<--");
    }

    @Override
    public String toString() {
        String info = "";
        info = "Color: "+color;
        info += "\nVelocidad maxima: "+vel_maxima;
        info += "\nModelo: "+modelo;
        info += "\nPlaca: "+placa+"\n";
        
        return info;
    }

}
