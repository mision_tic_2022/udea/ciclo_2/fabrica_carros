import java.util.Scanner;

public class Empresa {
    // ATRIBUTOS
    private String nombre;
    private String telefono;
    private String direccion;
    private String email;
    private String nit;
    private Carro[] carros;

    // CONSTRUCTOR
    public Empresa(String nombre, String telefono, String direccion, String email, String nit) {
        this.nombre = nombre;
        this.telefono = telefono;
        this.direccion = direccion;
        this.email = email;
        this.nit = nit;
        this.carros = new Carro[50];
    }

    /***************
     * CONSULTORES
     * GETTERS
     **************/

    public String getNombre() {
        return nombre;
    }

    public String getTelefono() {
        return telefono;
    }

    public String getDireccion() {
        return direccion;
    }

    public String getEmail() {
        return email;
    }

    public String getNit() {
        return nit;
    }

    /****************
     * MODIFICADORES
     * SETTERS
     ****************/

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    // ACCIONES
    public void fabricar_carro(String color, double vel_maxima, String placa) {
        // Crear un objeto de tipo Carro
        Carro carro = new Carro(color, vel_maxima, "2023", placa);
        int tamanio_arreglo = carros.length;
        int cont = 0;
        boolean flag = false;
        do {
            // Evaluar si no existe un carro en una posicion definida
            if (carros[cont] == null) {
                carros[cont] = carro;
                flag = true;
                //break;
            }
            cont++;
        } while (cont < tamanio_arreglo && !flag);//flag==false
    }

    public void solicitar_datos_carro(Scanner leer) {

            System.out.print("Ingrese el color que desea para el vehiculo: ");
            String color = leer.next();

            System.out.print("Ingrese la velocidad maxima que desea para el vehiculo(): ");
            double vel_maxima = leer.nextDouble();

            System.out.print("Ingrese placa del vehiculo: ");
            String placa = leer.next();

            fabricar_carro(color, vel_maxima, placa);
    }

    public void mostrar_vehiculos() {
        for(int i = 0; i < carros.length; i++){
            if(carros[i] != null){
                Carro carro = carros[i];
                System.out.println("---------------------------------------------------");
                System.out.println(carro);
                System.out.println("---------------------------------------------------");
            }
        }
    }

    public void menu() {
        String menu = "---------------------FABRICA DE CARROS--------------------\n";
        menu += "1) Fabricar vehiculo\n";
        menu += "2) Mostrar vehiculos\n";
        menu += "3) Salir\n";
        menu += ">>> ";

        int opc = 0;
        try (Scanner leer = new Scanner(System.in)) {
            
            do {
                opc = 0;

                System.out.print(menu);
                opc = leer.nextInt();
                leer.nextLine();
                // Evaluar la opción ingresada
                switch (opc) {
                    case 1:
                        solicitar_datos_carro(leer);
                        break;
                    case 2:
                        mostrar_vehiculos();
                        break;
                    case 3:
                        break;
                    default:
                        System.out.println("Por favor ingrese una opcion valida");
                }
            } while (opc != 3);
        } catch (Exception error) {
            System.out.println(error.getMessage());
            System.out.println("Por favor ingrese una opcion numerica");
        }
    }

}
